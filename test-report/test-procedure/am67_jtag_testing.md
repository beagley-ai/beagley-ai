Setup for testing:
=================

* TUMPA or equivalent dongles supported by OpenOCD. https://www.diygadget.com/JTAG-cables-and-microcontroller-programmers/tiao-usb-multi-protocol-adapter-JTAG-spi-i2c-serial
* Tag-Connect cable appropriate to the board such as TC2050-IDC-NL https://www.tag-connect.com/product/tc2050-arm2010-arm-20-pin-to-tc2050-adapter
* Tag-Connect to ARM20 adapter https://www.tag-connect.com/product/tc2050-arm2010-arm-20-pin-to-tc2050-adapter



WARNING: There is no point in getting the retainer clips https://www.tag-connect.com/product/tc2050-clip-3pack-retaining-clip since it wont fit in, you have to hold it.


S/w baseline:

openocd : master branch 04154af5d6cd jtag: linuxgpiod: drop extra parenthesis

Follow instruction for debugging for u-boot https://docs.u-boot.org/en/latest/board/beagle/am62x_beagleplay.html#debugging-u-boot

Config file:

```
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (C) 2024 Texas Instruments Incorporated - https://www.ti.com/
#
# Support for BeagleY-AI
#

# I use TUMPA: http://www.tiaowiki.com/w/TIAO_USB_Multi_Protocol_Adapter_User's_Manual
# https://www.diygadget.com/jtag-cables-and-microcontroller-programmers/tiao-usb-multi-protocol-adapter-jtag-spi-i2c-serial
source [find interface/ftdi/tumpa.cfg]

transport select jtag

# default JTAG configuration has only SRST and no TRST
reset_config srst_only srst_push_pull

# delay after SRST goes inactive
adapter srst delay 20

if { ![info exists SOC] } {
	set SOC j722s
}

source [find target/ti_k3.cfg]

ftdi tdo_sample_edge falling

# Speeds for FT2232H are in multiples of 2, and 32MHz is tops
# max speed we seem to achieve with BeagleY-AI is ~20MHz.. so we pick 16MHz
adapter speed 16000
```


Screenshots of working Setup
============================

WARNING: One has to hold the tag connect to be able to connect..
unfortunately the retainer clip wont fit.. so until a custom 3d
printed solution will probably work. but that solution is not in place
atm.


![BeagleY-AI JTAG](img/openocd_am67.jpg?raw=true "BeagleY-AI JTAG")
![BeagleY-AI OpenOCD Log](img/openocd_log_am67.jpg?raw=true "BeagleY-AI OpenOCD log")

